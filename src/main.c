#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static struct block_header* block_get_header(void* contents) {
	return (struct block_header*)(((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
	return mmap((void*)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

void* heap;

void test_1() {
	//first test
	printf("\nfirst test Normal successful memory allocation.\n");
	void* x = _malloc(123);
	void* y = _malloc(12);

	debug_heap(stdout, heap);

	struct block_header* x1 = block_get_header(x);
	struct block_header* y1 = block_get_header(y);

	if (x && y && !(*x1).is_free && !(*y1).is_free && (*x1).capacity.bytes == 123 && (*y1).capacity.bytes == 24)  printf("\nfirst test successfuly\n");
	else err("\nError test1\n");
	
	_free(y);
	_free(x);
	debug_heap(stdout, heap);
}

void test_2() {
	//second test
	printf("\nsecond test Freeing one block from several allocated ones.\n");

	void* x = _malloc(123);
	void* y = _malloc(12);

	debug_heap(stdout, heap);

	struct block_header* x1 = block_get_header(x);
	struct block_header* y1 = block_get_header(y);

	printf("\n\n");

		_free(y);

	debug_heap(stdout, heap);

	if (x && y && !(*x1).is_free && (*y1).is_free && (*x1).capacity.bytes == 123 )  printf("\nsecond test successfuly\n");
	else err("\nError test2\n");

	_free(x);
	debug_heap(stdout, heap);

}

void test_3() {
	printf("\nthird test Freeing two blocks from several allocated blocks.\n");

	void* x = _malloc(123);
	void* y = _malloc(121);
	void* z = _malloc(120);

	debug_heap(stdout, heap);

	struct block_header* x1 = block_get_header(x);
	struct block_header* y1 = block_get_header(y);

	printf("\n\n");

	_free(z);
	_free(y);

	debug_heap(stdout, heap);

	if (x && y && !(*x1).is_free && (*y1).is_free  && (*x1).capacity.bytes == 123 )  
		printf("\nthird test successfuly\n");
	else err("\nError test3\n");

	_free(x);
	debug_heap(stdout, heap);
	
}

void test_4() {
	printf("\nfourth test The memory has run out, the new memory region expands the old one.\n");

	void* x = _malloc(123);
	void* y = _malloc(10000);

	debug_heap(stdout, heap);

	struct block_header* x1 = block_get_header(x);
	struct block_header* y1 = block_get_header(y);

	if (x && y && !(*x1).is_free && !(*y1).is_free && (*x1).capacity.bytes == 123 && (*y1).capacity.bytes == 10000)  printf("\nfourth test successfuly\n");
	else err("\nError test4\n");

	_free(y);
	_free(x);
	
	debug_heap(stdout, heap);

}

void test_5(struct block_header* heap) {
	printf("\nfifth test The memory has run out, the old memory region cannot be expanded due to another allocated address range, the new region is allocated elsewhere.\n");

	void* x = _malloc(123);

	struct block_header* block = heap;

	while ((*block).next) { block = (*block).next; }
	void* addr = (*block).contents + (*block).capacity.bytes;

	map_pages(addr, 9999, MAP_FIXED);

	void* y = _malloc(10000);

	debug_heap(stdout, heap);

	struct block_header* x1 = block_get_header(x);
	struct block_header* y1 = block_get_header(y);

	if (x && y && !(*x1).is_free && !(*y1).is_free && (*x1).capacity.bytes == 123 && (*y1).capacity.bytes == 10000)  printf("\nfifth test successfuly\n");
	else err("\nError test5\n");

	_free(y);
	_free(x);
	
	debug_heap(stdout, heap);

}

	int main() {

		heap = heap_init(REGION_MIN_SIZE);
		debug_heap(stdout, heap);
		printf("\n");

		test_1();
		test_2();
		test_3();
		test_4();
		test_5(heap);
		
		printf("\nall tests passed\n");

		
	}
